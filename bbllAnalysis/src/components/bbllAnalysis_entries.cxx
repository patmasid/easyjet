#include "../BaselineVarsbbllAlg.h"
#include "../MMCDecoratorAlg.h"
#include "../HHbbllSelectorAlg.h"
#include "../NeutrinoWeightingAlg.h"
#include "../NeutrinoWeightingTool.h"


using namespace HHBBLL;

DECLARE_COMPONENT(BaselineVarsbbllAlg)
DECLARE_COMPONENT(MMCDecoratorAlg)
DECLARE_COMPONENT(HHbbllSelectorAlg)
DECLARE_COMPONENT(NeutrinoWeightingAlg)
DECLARE_COMPONENT(NeutrinoWeightingTool)

#include "../BaselineVarsttHHAlg.h"
#include "../JetPairingAlgttHH.h"
#include "../ttHHSelectorAlg.h"
#include "../TriggerDecoratorAlg.h"

using namespace ttHH;

DECLARE_COMPONENT(BaselineVarsttHHAlg)
DECLARE_COMPONENT(JetPairingAlgttHH)
DECLARE_COMPONENT(ttHHSelectorAlg)
DECLARE_COMPONENT(TriggerDecoratorAlg)
